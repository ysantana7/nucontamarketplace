class OffersResponseObject {
  static final dbId = 'id';
  static final dbName = 'name';
  static final dbBalance = 'balance';
  static final dbOffers = 'offers';

  final String id;
  final String name;
  final int balance;
  final List<OfferObject> offerObjectList;

  OffersResponseObject({
    required this.id,
    required this.name,
    required this.balance,
    required this.offerObjectList,
  });

  factory OffersResponseObject.fromJson({
    required Map<String, dynamic> json,
  }) =>
      OffersResponseObject(
        id: json[dbId] ?? '',
        name: json[dbName] ?? '',
        balance: json[dbBalance] ?? 0,
        offerObjectList: (json[dbOffers] == null)
            ? []
            : OfferObject.toList(
                json: json[dbOffers],
              ),
      );
}

class OfferObject {
  static final dbId = 'id';
  static final dbPrice = 'price';
  static final dbProduct = 'product';

  final String id;
  final int price;
  final ProductObject? productObject;

  OfferObject({
    required this.id,
    required this.price,
    required this.productObject,
  });

  static List<OfferObject> toList({
    required List<dynamic> json,
  }) {
    return json.map((item) {
      return OfferObject.fromJson(
        json: item,
      );
    }).toList();
  }

  factory OfferObject.fromJson({
    required Map<String, dynamic> json,
  }) =>
      OfferObject(
        id: json[dbId] ?? '',
        price: json[dbPrice] ?? 0,
        productObject: (json[dbProduct] == null)
            ? null
            : ProductObject.fromJson(
                json: json[dbProduct],
              ),
      );
}

class ProductObject {
  static final dbId = 'id';
  static final dbName = 'name';
  static final dbDescription = 'description';
  static final dbImage = 'image';

  final String id;
  final String name;
  final String description;
  final String image;

  ProductObject({
    required this.id,
    required this.name,
    required this.description,
    required this.image,
  });

  factory ProductObject.fromJson({
    required Map<String, dynamic> json,
  }) =>
      ProductObject(
        id: json[dbId] ?? '',
        name: json[dbName] ?? '',
        description: json[dbDescription] ?? '',
        image: json[dbImage] ?? '',
      );
}
