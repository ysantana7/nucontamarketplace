import 'package:nucontamarketplace/models/offers/response_objects/offers_response_object.dart';

class OfferDetailResponseObject {
  static final dbSuccess = 'success';
  static final dbErrorMessage = 'errorMessage';
  static final dbCustomer = 'customer';

  final bool success;
  final String errorMessage;
  final OffersResponseObject? customerObject;

  OfferDetailResponseObject({
    required this.success,
    required this.errorMessage,
    required this.customerObject,
  });

  factory OfferDetailResponseObject.fromJson({
    required Map<String, dynamic> json,
  }) {
    final purchase = json['purchase'];
    return OfferDetailResponseObject(
      success: purchase[dbSuccess] ?? false,
      errorMessage: purchase[dbErrorMessage] ?? '',
      customerObject: (purchase[dbCustomer] == null)
          ? null
          : OffersResponseObject.fromJson(
              json: purchase[dbCustomer],
            ),
    );
  }
}
