Map<String, String> getSpanishLocalizations() {
  return {
    'app_name': 'NuConta Marketplace',
    'unknown_error':
        'Ocurrió un error inesperado, por favor intenta más tarde.',
    'offer': 'Oferta',
    'price': 'Precio',
    'pay': 'Pagar',
    'paying': 'Pagando',
    'purchase_successful': 'La compra se realizó exitosamente.',
    'successful': 'Exitoso',
    'error': 'Error',
    'accept': 'Aceptar',
    'balance': 'Saldo'
  };
}
