import 'package:flutter/material.dart';

import 'package:nucontamarketplace/localization/en_localization.dart';
import 'package:nucontamarketplace/localization/es_localization.dart';

class NuLocalizations {
  final Locale locale;

  NuLocalizations({
    required this.locale,
  });

  static NuLocalizations of({
    required BuildContext context,
  }) =>
      Localizations.of<NuLocalizations>(
        context,
        NuLocalizations,
      )!;

  final Map<String, Map<String, String>> _localizedValues = {
    'en': getEnglishLocalizations(),
    'es': getSpanishLocalizations(),
  };

  String getLocaleMessage({
    required String key,
  }) =>
      this._localizedValues[locale.languageCode]![key]!;
}
