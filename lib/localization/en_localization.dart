Map<String, String> getEnglishLocalizations() {
  return {
    'app_name': 'NuConta Marketplace',
    'unknown_error': 'An unexpected error occurred, please try again later.',
    'offer': 'Offer',
    'price': 'Price',
    'pay': 'Pay',
    'paying': 'Paying',
    'purchase_successful': 'The purchase was successful.',
    'successful': 'Successful',
    'error': 'Error',
    'accept': 'Accept',
    'balance': 'Balance'
  };
}
