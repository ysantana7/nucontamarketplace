import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';

import 'package:nucontamarketplace/localization/nu_localizations.dart';

class NuLocalizationsDelegate extends LocalizationsDelegate<NuLocalizations> {
  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<NuLocalizations> load(Locale locale) =>
      SynchronousFuture<NuLocalizations>(NuLocalizations(
        locale: locale,
      ));

  @override
  bool shouldReload(_) => false;
}
