import 'package:flutter/material.dart';

import 'package:graphql_flutter/graphql_flutter.dart';

//Providers
import 'package:nucontamarketplace/providers/graphql_client_provider.dart';
import 'package:provider/provider.dart';

//Localizations
import 'package:nucontamarketplace/localization/nu_localizations_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//Routes
import 'package:nucontamarketplace/ui/routes/generate_routes.dart';
import 'package:nucontamarketplace/ui/routes/nu_routes.dart';

//Utils
import 'package:nucontamarketplace/ui/utils/nu_colors.dart';

void main() async {
  await initHiveForFlutter();

  final multiProvider = MultiProvider(
    providers: [
      Provider<GraphqlClientProvider>(
        create: (_) => GraphqlClientProvider(),
      ),
    ],
    child: MyApp(),
  );

  runApp(multiProvider);
}

class MyApp extends StatelessWidget {
  final _key = UniqueKey();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: this._key,
      title: 'NuConta Marketplace',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        // ... app-specific localization delegates
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        NuLocalizationsDelegate()
      ],
      supportedLocales: [
        const Locale.fromSubtags(
          languageCode: 'en',
          countryCode: 'US',
        ),
        const Locale.fromSubtags(
          languageCode: 'es',
          countryCode: 'MX',
        ),
      ],
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: NuColors.backgroundColorPurple,
        accentColor: NuColors.backgroundColorPurple,
      ),
      initialRoute: NuRoutes.SPLASH_SCREEN,
      onGenerateRoute: onGenerateRoute,
    );
  }
}
