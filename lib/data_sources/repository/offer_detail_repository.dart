import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:nucontamarketplace/providers/graphql_client_provider.dart';

//Queries
import 'package:nucontamarketplace/data_sources/queries/offers_querie.dart';

//Models
import 'package:nucontamarketplace/models/offer_detail/response_objects/offer_detail_response_object.dart';

class OfferDetailRepository {
  final GraphqlClientProvider _client;

  OfferDetailRepository({
    required GraphqlClientProvider client,
  }) : this._client = client;

  Future<OfferDetailResponseObject> payOffer({
    required String offerId,
  }) {
    final options = WatchQueryOptions(
      document: gql(OffersQuery.payOffer),
      fetchResults: true,
      variables: {
        'offerId': offerId,
      },
    );
    return this._client.graphQLClient.query(options).then(
          (value) => OfferDetailResponseObject.fromJson(
            json: value.data!,
          ),
        );
  }
}
