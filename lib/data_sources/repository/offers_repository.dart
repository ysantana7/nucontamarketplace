import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:nucontamarketplace/providers/graphql_client_provider.dart';

//Queries
import 'package:nucontamarketplace/data_sources/queries/offers_querie.dart';

//Models
import 'package:nucontamarketplace/models/offers/response_objects/offers_response_object.dart';

class OffersRepository {
  final GraphqlClientProvider _client;

  OffersRepository({
    required GraphqlClientProvider client,
  }) : this._client = client;

  Future<OffersResponseObject> getOffers() {
    final options = WatchQueryOptions(
      document: gql(OffersQuery.getOffers),
      fetchResults: true,
    );
    return this._client.graphQLClient.query(options).then(
          (value) => OffersResponseObject.fromJson(
            json: value.data!['viewer'],
          ),
        );
  }
}
