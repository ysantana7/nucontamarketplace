abstract class OffersQuery {
  static final String getOffers = r""" 
    {
      viewer {
        id
        name
        balance
        offers {
          id
          price
          product {
            id
            name
            description
            image
          }
        }
      }
    }
  """;

  static final String payOffer = r""" 
    mutation Payoffer($offerId: ID!) {
      purchase(offerId: $offerId) {
        success
        errorMessage
        customer {
          id
          name
          balance
          offers {
            id
            price
            product {
              id
              name
              description
              image
            }
          }
        }
      }
    }
  """;
}
