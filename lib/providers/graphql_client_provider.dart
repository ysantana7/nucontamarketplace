import 'package:graphql_flutter/graphql_flutter.dart';

//Utils
import 'package:nucontamarketplace/data_sources/constants.dart';

class GraphqlClientProvider {
  final GraphQLClient graphQLClient;

  GraphqlClientProvider._internal({
    required this.graphQLClient,
  });

  factory GraphqlClientProvider() {
    final HttpLink httpLink = HttpLink(Constants.url);

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer ${Constants.token}',
    );
    final Link gqlLink = authLink.concat(httpLink);

    final GraphQLClient graphQLClient = GraphQLClient(
      cache: GraphQLCache(store: HiveStore()),
      link: gqlLink,
    );

    return GraphqlClientProvider._internal(
      graphQLClient: graphQLClient,
    );
  }
}
