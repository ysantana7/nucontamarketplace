import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:cached_network_image/cached_network_image.dart';

//Blocs
import 'package:nucontamarketplace/blocs/status_stream.dart';
import 'package:nucontamarketplace/blocs/offers_bloc.dart';

//Models
import 'package:nucontamarketplace/models/offers/response_objects/offers_response_object.dart';

//notifiers
import 'package:nucontamarketplace/ui/offers/notifiers/balance_notifier.dart';

//Animations
import 'package:nucontamarketplace/ui/widgets/animations/nu_fade_in_down.dart';

//Widgets
import 'package:nucontamarketplace/ui/widgets/nu_progress_indicator.dart';
import 'package:nucontamarketplace/ui/offers/widgets/balance_widget.dart';
import 'package:nucontamarketplace/ui/widgets/nu_exception.dart';

//Utils
import 'package:nucontamarketplace/localization/nu_localizations.dart';
import 'package:nucontamarketplace/ui/routes/nu_routes.dart';
import 'package:nucontamarketplace/ui/utils/nu_util.dart';

class OffersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final nuLocalizations = NuLocalizations.of(context: context);

    final offersBloc = Provider.of<OffersBloc>(
      context,
      listen: false,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          nuLocalizations.getLocaleMessage(
            key: 'app_name',
          ),
        ),
      ),
      body: StreamBuilder<StatusStream<OffersResponseObject>>(
        stream: offersBloc.offersStream,
        builder: (_, snapshot) {
          if (!snapshot.hasData) {
            return buildProgressCenter();
          } else if (snapshot.hasError) {
            return buildErrorCenter(
              message: snapshot.error.toString(),
            );
          } else {
            final data = snapshot.data!;
            switch (data.status) {
              case Status.LOADING:
                return buildProgressCenter();
              case Status.SUCCESS:
                return ChangeNotifierProvider<BalanceNotifier>(
                  create: (_) => BalanceNotifier(
                    name: data.data!.name,
                    balance: data.data!.balance,
                  ),
                  child: Stack(
                    children: [
                      _OfferItemList(
                        offerObjectList: data.data!.offerObjectList,
                      ),
                      this._buildBalance(),
                    ],
                  ),
                );
              case Status.ERROR:
                return buildErrorCenter(
                  message: data.message,
                );
              default:
                return buildErrorCenter(
                  message: nuLocalizations.getLocaleMessage(
                    key: 'unknown_error',
                  ),
                );
            }
          }
        },
      ),
    );
  }

  Widget _buildBalance() => Positioned(
        left: 0,
        right: 0,
        bottom: 0,
        child: BalanceWidget(),
      );
}

class _OfferItemList extends StatelessWidget {
  final List<OfferObject> offerObjectList;

  const _OfferItemList({
    required this.offerObjectList,
  });

  @override
  Widget build(BuildContext context) => ListView.separated(
        itemCount: this.offerObjectList.length,
        padding: const EdgeInsets.only(
          top: 16.0,
          left: 16.0,
          right: 16.0,
          bottom: 100.0,
        ),
        separatorBuilder: (_, __) => Container(height: 10),
        itemBuilder: (_, index) => NuFadeInDown(
          child: _OfferItem(
            offerObject: this.offerObjectList[index],
          ),
        ),
      );
}

class _OfferItem extends StatelessWidget {
  final OfferObject offerObject;

  const _OfferItem({
    required this.offerObject,
  });

  void _onTap({
    required BuildContext context,
  }) async {
    final result = await Navigator.pushNamed(
      context,
      NuRoutes.OFFER_DETAIL_SCREEN,
      arguments: {
        'offerObject': this.offerObject,
      },
    );
    if (result != null) {
      Provider.of<BalanceNotifier>(
        context,
        listen: false,
      ).updateBalance(
        balance: (result as int),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final nuLocalizations = NuLocalizations.of(context: context);
    return InkWell(
      onTap: () => this._onTap(
        context: context,
      ),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 14.0,
          ),
          child: Row(
            children: [
              this._buildImage(),
              const SizedBox(width: 10.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    this._buildOfferTitle(
                      nuLocalizations: nuLocalizations,
                    ),
                    this._buildName(),
                    const SizedBox(height: 10.0),
                    this._buildDescription(),
                    const SizedBox(height: 12.0),
                    this._buildPrice(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImage() => SizedBox(
        width: 100,
        child: Hero(
          tag: this.offerObject.id,
          child: CachedNetworkImage(
            imageUrl: this.offerObject.productObject!.image,
            placeholder: (_, __) => buildProgressCenter(),
            errorWidget: (_, __, ___) => const Icon(Icons.error),
            fadeInDuration: const Duration(
              milliseconds: 1500,
            ),
          ),
        ),
      );

  Widget _buildOfferTitle({
    required NuLocalizations nuLocalizations,
  }) =>
      Container(
        color: Colors.red,
        padding: const EdgeInsets.symmetric(
          horizontal: 4.0,
          vertical: 2.0,
        ),
        child: Text(
          nuLocalizations.getLocaleMessage(
            key: 'offer',
          ),
          style: const TextStyle(
            fontSize: 10.0,
            color: Colors.white,
          ),
        ),
      );

  Widget _buildName() => Text(
        this.offerObject.productObject!.name,
        style: const TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      );

  Widget _buildDescription() => Text(
        this.offerObject.productObject!.description,
        style: const TextStyle(
          fontSize: 13.0,
          color: Colors.grey,
        ),
      );

  Widget _buildPrice() => Text(
        NuUtil.simpleCurrency(
          value: this.offerObject.price,
        ),
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      );
}
