import 'package:flutter/foundation.dart';

class BalanceNotifier with ChangeNotifier {
  final String name;
  int balance;

  BalanceNotifier({
    required this.name,
    required this.balance,
  });

  void updateBalance({
    required int balance,
  }) {
    this.balance = balance;
    notifyListeners();
  }
}
