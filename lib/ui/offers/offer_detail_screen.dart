import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:cached_network_image/cached_network_image.dart';

//Blocs
import 'package:nucontamarketplace/blocs/offer_detail_bloc.dart';
import 'package:nucontamarketplace/blocs/status_stream.dart';

//Models
import 'package:nucontamarketplace/models/offer_detail/response_objects/offer_detail_response_object.dart';
import 'package:nucontamarketplace/models/offers/response_objects/offers_response_object.dart';

//Widgets
import 'package:nucontamarketplace/ui/widgets/nu_progress_indicator.dart';
import 'package:nucontamarketplace/ui/widgets/nu_dialog.dart';
import 'package:nucontamarketplace/ui/widgets/nu_button.dart';

//Utils
import 'package:nucontamarketplace/localization/nu_localizations.dart';
import 'package:nucontamarketplace/ui/utils/nu_util.dart';

class OfferDetailScreen extends StatelessWidget {
  final OfferObject offerObject;

  const OfferDetailScreen({
    required this.offerObject,
  });

  void _showDialogSuccess({
    required BuildContext context,
    required NuLocalizations nuLocalizations,
    required int balance,
  }) async {
    final dialogResult = await NuDialog.show(
      context: context,
      nuLocalizations: nuLocalizations,
      title: nuLocalizations.getLocaleMessage(
        key: 'successful',
      ),
      description: nuLocalizations.getLocaleMessage(
        key: 'purchase_successful',
      ),
      onAccept: (contextDialog) {
        Navigator.pop(contextDialog, ConfirmAction.ACCEPT);
      },
    );
    if (dialogResult == ConfirmAction.ACCEPT) {
      Navigator.pop(context, balance);
    }
  }

  void _initListeners({
    required BuildContext context,
    required NuLocalizations nuLocalizations,
    required OfferDetailBloc offerDetailBloc,
  }) {
    offerDetailBloc.payOfferStream.listen((event) {
      if (event.status == Status.SUCCESS) {
        final data = event.data!;
        this._showDialogSuccess(
          context: context,
          nuLocalizations: nuLocalizations,
          balance: data.customerObject!.balance,
        );
      } else if (event.status == Status.ERROR) {
        NuDialog.show(
          context: context,
          nuLocalizations: nuLocalizations,
          title: nuLocalizations.getLocaleMessage(
            key: 'error',
          ),
          description: event.message,
          onAccept: (contextDialog) {
            Navigator.pop(contextDialog);
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final nuLocalizations = NuLocalizations.of(context: context);

    final offerDetailBloc = Provider.of<OfferDetailBloc>(
      context,
      listen: false,
    );

    this._initListeners(
      context: context,
      nuLocalizations: nuLocalizations,
      offerDetailBloc: offerDetailBloc,
    );

    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          this._buildHeader(
            size: size,
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Expanded(
                    child: this._buildContent(
                      nuLocalizations: nuLocalizations,
                    ),
                  ),
                  this._buildPayButton(
                    nuLocalizations: nuLocalizations,
                    offerDetailBloc: offerDetailBloc,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader({
    required Size size,
  }) =>
      SliverAppBar(
        title: Text(
          this.offerObject.productObject!.name,
        ),
        pinned: true,
        floating: true,
        flexibleSpace: Hero(
          tag: this.offerObject.id,
          child: CachedNetworkImage(
            fit: BoxFit.cover,
            imageUrl: this.offerObject.productObject!.image,
            placeholder: (_, __) => buildProgressCenter(),
            errorWidget: (_, __, ___) => const Icon(Icons.error),
            fadeInDuration: const Duration(
              milliseconds: 1500,
            ),
          ),
        ),
        expandedHeight: (size.height * .30),
      );

  Widget _buildContent({
    required NuLocalizations nuLocalizations,
  }) =>
      Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "${nuLocalizations.getLocaleMessage(key: 'price')}: ",
                style: const TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                NuUtil.simpleCurrency(
                  value: this.offerObject.price,
                ),
                style: const TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          Text(
            this.offerObject.productObject!.description,
            style: const TextStyle(
              fontSize: 20.0,
            ),
          ),
        ],
      );

  Widget _buildPayButton({
    required NuLocalizations nuLocalizations,
    required OfferDetailBloc offerDetailBloc,
  }) =>
      StreamBuilder<StatusStream<OfferDetailResponseObject>>(
        stream: offerDetailBloc.payOfferStream,
        builder: (_, snapshot) {
          final isLoading =
              (snapshot.hasData && (snapshot.data?.status == Status.LOADING));

          if (isLoading) {
            return this._buildProgress(
              nuLocalizations: nuLocalizations,
            );
          } else {
            return NuButton(
              title: nuLocalizations.getLocaleMessage(
                key: 'pay',
              ),
              onTap: () => offerDetailBloc.payOffer(
                offerId: this.offerObject.id,
              ),
            );
          }
        },
      );

  Widget _buildProgress({
    required NuLocalizations nuLocalizations,
  }) =>
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          buildProgressCenter(),
          const SizedBox(width: 10.0),
          Text(
            '${nuLocalizations.getLocaleMessage(key: 'paying')}...',
          ),
        ],
      );
}
