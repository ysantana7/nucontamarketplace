import 'package:flutter/material.dart';

//Notifiers
import 'package:nucontamarketplace/ui/offers/notifiers/balance_notifier.dart';

//Utils
import 'package:nucontamarketplace/ui/routes/generate_route_imports.dart';
import 'package:nucontamarketplace/localization/nu_localizations.dart';
import 'package:nucontamarketplace/ui/utils/nu_util.dart';

class BalanceWidget extends StatelessWidget {
  const BalanceWidget();

  @override
  Widget build(BuildContext context) {
    final nuLocalizations = NuLocalizations.of(context: context);

    final balanceNotifier = Provider.of<BalanceNotifier>(
      context,
      listen: false,
    );

    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(
        top: 15.0,
        bottom: 15.0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: const Radius.circular(30.0),
          topRight: const Radius.circular(30.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black87,
            blurRadius: 4,
            offset: Offset(1, .3),
          ),
        ],
      ),
      child: Column(
        children: [
          Text(
            balanceNotifier.name,
            style: const TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10.0),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "${nuLocalizations.getLocaleMessage(
                  key: 'balance',
                )}:",
                style: const TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 7.0),
              Consumer<BalanceNotifier>(
                builder: (_, notifier, __) {
                  final balance = NuUtil.simpleCurrency(
                    value: notifier.balance,
                  );
                  return Text(
                    balance,
                    style: const TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
