import 'package:flutter/material.dart';

//Utils
import 'package:nucontamarketplace/localization/nu_localizations.dart';

enum ConfirmAction { CANCEL, ACCEPT }

class NuDialog {
  static Future<ConfirmAction> show<ConfirmAction>({
    required BuildContext context,
    required NuLocalizations nuLocalizations,
    required String title,
    required String description,
    required Function(BuildContext) onAccept,
  }) async {
    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (contextDialog) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(title),
            content: Text(description),
            actions: [
              SimpleDialogOption(
                child: Text(
                  nuLocalizations.getLocaleMessage(
                    key: 'accept',
                  ),
                ),
                onPressed: () => onAccept(contextDialog),
              )
            ],
          ),
        );
      },
    );
  }
}
