import 'package:flutter/material.dart';

class NuException extends StatelessWidget {
  final String message;

  const NuException({
    required this.message,
  });

  @override
  Widget build(BuildContext context) => Text(
        this.message,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 16.0,
        ),
      );
}

Widget buildErrorCenter({
  required String message,
}) =>
    Center(
      child: NuException(
        message: message,
      ),
    );
