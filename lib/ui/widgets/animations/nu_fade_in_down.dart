import 'package:flutter/material.dart';

class NuFadeInDown extends StatefulWidget {
  final Widget child;
  final Duration duration;
  final Function(AnimationController)? controller;
  final double from;

  const NuFadeInDown({
    required this.child,
    this.duration = const Duration(milliseconds: 700),
    this.controller,
    this.from = 100,
  });

  @override
  _NuFadeInDownState createState() => _NuFadeInDownState();
}

class _NuFadeInDownState extends State<NuFadeInDown>
    with SingleTickerProviderStateMixin {
  AnimationController? _controller;
  late Animation<double> _animation;
  late Animation<double> _opacity;

  @override
  void dispose() {
    this._controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    this._controller = AnimationController(
      duration: widget.duration,
      vsync: this,
    );

    this._animation = Tween<double>(
      begin: widget.from * -1,
      end: 0,
    ).animate(CurvedAnimation(
      parent: this._controller!,
      curve: Curves.easeOut,
    ));

    this._opacity = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: this._controller!,
      curve: const Interval(0, 0.65),
    ));

    this._controller?.forward();

    if (widget.controller != null) {
      widget.controller!(this._controller!);
    }
  }

  @override
  Widget build(BuildContext context) => AnimatedBuilder(
        child: widget.child,
        animation: this._controller!,
        builder: (_, child) => Transform.translate(
          offset: Offset(0, this._animation.value),
          child: Opacity(
            opacity: this._opacity.value,
            child: child,
          ),
        ),
      );
}
