import 'package:flutter/material.dart';

//Utils
import 'package:nucontamarketplace/ui/utils/nu_colors.dart';

class NuButton extends StatelessWidget {
  final String title;
  final Function() onTap;

  const NuButton({
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) => SizedBox(
        height: 48,
        width: double.infinity,
        child: Material(
          clipBehavior: Clip.hardEdge,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: MaterialButton(
            color: NuColors.backgroundColorPurple,
            onPressed: onTap,
            child: Text(
              this.title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
      );
}
