import 'package:flutter/material.dart';

//Utils
import 'package:nucontamarketplace/ui/utils/nu_colors.dart';

Widget buildProgressCenter() => Center(
      child: const CircularProgressIndicator(
        valueColor:
            AlwaysStoppedAnimation<Color>(NuColors.backgroundColorPurple),
      ),
    );
