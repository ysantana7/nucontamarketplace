import 'package:flutter/material.dart';

class SlideLeftRoute extends PageRouteBuilder {
  final RouteSettings settings;
  final Widget Function(BuildContext, Animation<double>, Animation<double>)
      pageBuilder;

  SlideLeftRoute({
    required this.settings,
    required this.pageBuilder,
  }) : super(
          opaque: false,
          settings: settings,
          pageBuilder: pageBuilder,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        );
}
