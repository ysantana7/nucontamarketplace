import 'package:flutter/material.dart';

class FadeRoute extends PageRouteBuilder {
  final RouteSettings settings;
  final Widget Function(BuildContext, Animation<double>, Animation<double>)
      pageBuilder;

  FadeRoute({
    required this.settings,
    required this.pageBuilder,
  }) : super(
          opaque: false,
          settings: settings,
          pageBuilder: pageBuilder,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
