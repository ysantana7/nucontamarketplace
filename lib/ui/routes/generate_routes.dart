import 'package:flutter/material.dart';

import 'package:nucontamarketplace/ui/routes/generate_route_imports.dart';

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    case NuRoutes.SPLASH_SCREEN:
      return SlideLeftRoute(
        settings: settings,
        pageBuilder: (routeContext, _, __) => SplashScreen(),
      );
    case NuRoutes.OFFERS_SCREEN:
      return SlideLeftRoute(
        settings: settings,
        pageBuilder: (routeContext, _, __) => Provider<OffersBloc>(
          create: (_) {
            final bloc = generateOffersBloc(
              routeContext: routeContext,
            );
            bloc.getOffers();
            return bloc;
          },
          child: OffersScreen(),
        ),
      );
    case NuRoutes.OFFER_DETAIL_SCREEN:
      final args = settings.arguments as Map;
      final OfferObject offerObject = args['offerObject'];
      return SlideLeftRoute(
        settings: settings,
        pageBuilder: (routeContext, _, __) => Provider<OfferDetailBloc>(
          create: (_) => generateOfferDetailBloc(
            routeContext: routeContext,
          ),
          child: OfferDetailScreen(
            offerObject: offerObject,
          ),
        ),
      );
    default:
      throw Exception('Invalid kn route name');
  }
}
