abstract class NuRoutes {
  static const String SPLASH_SCREEN = 'splash_screen';
  static const String OFFERS_SCREEN = 'offers_screen';
  static const String OFFER_DETAIL_SCREEN = 'offer_detail_screen';
}
