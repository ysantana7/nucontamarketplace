import 'package:flutter/material.dart';

//Utils
import 'package:nucontamarketplace/localization/nu_localizations.dart';
import 'package:nucontamarketplace/ui/routes/nu_routes.dart';
import 'package:nucontamarketplace/ui/utils/nu_colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void _goToScreen({
    required String route,
  }) =>
      Navigator.pushNamedAndRemoveUntil(
        context,
        route,
        (Route<dynamic> route) => false,
      );

  void _initTimer() {
    Future.delayed(const Duration(seconds: 3), () {
      this._goToScreen(route: NuRoutes.OFFERS_SCREEN);
    });
  }

  @override
  void initState() {
    super.initState();
    this._initTimer();
  }

  @override
  Widget build(BuildContext context) {
    final nuLocalizations = NuLocalizations.of(context: context);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: NuColors.backgroundGradient,
        ),
        child: Center(
          child: Text(
            nuLocalizations.getLocaleMessage(
              key: 'app_name',
            ),
            style: const TextStyle(
              fontSize: 25.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
