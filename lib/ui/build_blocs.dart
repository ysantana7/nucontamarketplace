import 'package:flutter/material.dart';

//Providers
import 'package:nucontamarketplace/providers/graphql_client_provider.dart';
import 'package:provider/provider.dart';

//Repositories
import 'package:nucontamarketplace/data_sources/repository/offer_detail_repository.dart';
import 'package:nucontamarketplace/data_sources/repository/offers_repository.dart';

//Blocs
import 'package:nucontamarketplace/blocs/offer_detail_bloc.dart';
import 'package:nucontamarketplace/blocs/offers_bloc.dart';

OffersBloc generateOffersBloc({
  required BuildContext routeContext,
}) {
  //Graphql Client
  final client = Provider.of<GraphqlClientProvider>(
    routeContext,
    listen: false,
  );

  //Repositories
  final offersRepository = OffersRepository(
    client: client,
  );

  return OffersBloc(
    offersRepository: offersRepository,
  );
}

OfferDetailBloc generateOfferDetailBloc({
  required BuildContext routeContext,
}) {
  //Graphql Client
  final client = Provider.of<GraphqlClientProvider>(
    routeContext,
    listen: false,
  );

  //Repositories
  final offerDetailRepository = OfferDetailRepository(
    client: client,
  );

  return OfferDetailBloc(
    offerDetailRepository: offerDetailRepository,
  );
}
