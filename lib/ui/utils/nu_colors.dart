import 'package:flutter/material.dart';

abstract class NuColors {
  static const backgroundColorPurple = const Color.fromRGBO(129, 33, 205, 1);

  static const Gradient backgroundGradient = LinearGradient(
    colors: [
      const Color.fromRGBO(39, 28, 80, 1),
      const Color.fromRGBO(60, 40, 100, 1),
    ],
  );
}
