import 'package:intl/intl.dart';

class NuUtil {
  static String simpleCurrency({
    required int value,
  }) =>
      '${NumberFormat.simpleCurrency(locale: 'es_MX').format(value)}';
}
