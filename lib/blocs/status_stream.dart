enum Status {
  LOADING,
  SUCCESS,
  ERROR,
}

class StatusStream<T> {
  final Status? status;
  T? data;
  final String? msg;

  String get message => this.msg ?? '';

  StatusStream({
    required this.status,
    this.data,
    required this.msg,
  });

  bool get hasData => data != null;

  StatusStream.success(this.data)
      : status = Status.SUCCESS,
        this.msg = null;

  StatusStream.loading()
      : status = Status.LOADING,
        data = null,
        this.msg = null;

  StatusStream.error(this.msg)
      : status = Status.ERROR,
        data = null;
}
