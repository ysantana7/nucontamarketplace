import 'package:rxdart/rxdart.dart';

//Repositories
import 'package:nucontamarketplace/data_sources/repository/offers_repository.dart';

//Blocs
import 'package:nucontamarketplace/blocs/status_stream.dart';
import 'package:nucontamarketplace/blocs/base_bloc.dart';

//Models
import 'package:nucontamarketplace/models/offers/response_objects/offers_response_object.dart';

class OffersBloc extends BaseBloc {
  final OffersRepository _offersRepository;

  OffersBloc({
    required OffersRepository offersRepository,
  }) : this._offersRepository = offersRepository;

  //Subjects
  // ignore: close_sinks
  final _offersSubject = BehaviorSubject<StatusStream<OffersResponseObject>>();

  //Streams
  Stream<StatusStream<OffersResponseObject>> get offersStream =>
      this._offersSubject.stream;

  //Sinks
  Function(StatusStream<OffersResponseObject>) get _offersSink =>
      this._offersSubject.sink.add;

  void getOffers() async {
    try {
      this._offersSink(StatusStream.loading());

      final offersResponseObject = await this._offersRepository.getOffers();

      if (this.isNotClosed()) {
        this._offersSink(StatusStream.success(offersResponseObject));
      }
    } catch (error) {
      this._offersSink(StatusStream.error(error.toString()));
    }
  }

  @override
  void dispose() {
    this._offersSubject.close();
  }

  @override
  bool isNotClosed() => !this._offersSubject.isClosed;
}
