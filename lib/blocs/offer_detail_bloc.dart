import 'package:rxdart/rxdart.dart';

//Repositories
import 'package:nucontamarketplace/data_sources/repository/offer_detail_repository.dart';

//Models
import 'package:nucontamarketplace/models/offer_detail/response_objects/offer_detail_response_object.dart';

//Blocs
import 'package:nucontamarketplace/blocs/status_stream.dart';
import 'package:nucontamarketplace/blocs/base_bloc.dart';

class OfferDetailBloc extends BaseBloc {
  final OfferDetailRepository _offerDetailRepository;

  OfferDetailBloc({
    required OfferDetailRepository offerDetailRepository,
  }) : this._offerDetailRepository = offerDetailRepository;

  //Subjects
  // ignore: close_sinks
  final _payOfferSubject =
      BehaviorSubject<StatusStream<OfferDetailResponseObject>>();

  //Streams
  Stream<StatusStream<OfferDetailResponseObject>> get payOfferStream =>
      this._payOfferSubject.stream;

  //Sinks
  Function(StatusStream<OfferDetailResponseObject>) get _payOfferSink =>
      this._payOfferSubject.sink.add;

  void payOffer({
    required String offerId,
  }) async {
    try {
      this._payOfferSink(StatusStream.loading());

      final offersResponseObject = await this._offerDetailRepository.payOffer(
            offerId: offerId,
          );

      if (this.isNotClosed()) {
        if (offersResponseObject.success) {
          this._payOfferSink(StatusStream.success(offersResponseObject));
        } else {
          this._payOfferSink(
              StatusStream.error(offersResponseObject.errorMessage));
        }
      }
    } catch (error) {
      this._payOfferSink(StatusStream.error(error.toString()));
    }
  }

  @override
  void dispose() {
    this._payOfferSubject.close();
  }

  @override
  bool isNotClosed() => !this._payOfferSubject.isClosed;
}
