# nucontamarketplace

## Installation required
[Install Flutter 2](https://flutter.dev/docs/get-started/install)

## Project installation
`git clone https://ysantana7@bitbucket.org/ysantana7/nucontamarketplace.git`

`cd nucontamarketplace`

`flutter packages get`

## Run project
Enter the folder with:

`cd nucontamarketplace`

**Android:**

To compile and install the application on android run the following.

`flutter run lib/main.dart`

**IOS:**

Register app on App Store Connect:
```
1- First you will need to locate the Runner.xcworkspace file inside the ios folder of your project folder.
2- After you opened your project, you can configure the device on the top left corner. Make sure your iPhone is connected to your computer with an USB cable.
3- You can then find your device after you bring up the menu, I used my iPhone 8 for this example.
4- In the Signing configuration you need to select a team.
5- I used personal team or one designated developer.
6- You will likely run into an error because you have an invalid Bundle Identifier.
7- To fix this, you need to modify your Bundle Identifier to something that is unique. I used com.example2021.tuCasaExpressFlutter for this example.
8- After that you can click run and build the project.
9- You might be prompted to enter your keychain, click always allow to save yourself some efforts.
10- After that the app should be successfully built onto your device. Before you test it, you also have to trust yourself as the developer on that device.
11- You need to go to your Settings > General > Device Management.
12- Inside Device Management, select the developer name and tap Trust “YOUR DEVELOPER NAME”.
13- You should now be able to run your Flutter app on your local device.
```

## Folder Structure
Main folder structure that flutter provides.

```
nucontamarketplace/
|- android
|- build
|- ios
|- lib
|- test
|- web
```

Folder structure that is being used in this project.

```
lib/
|- src
    |- blocs/
    |- data_sources/
    |- localization/
    |- models/
    |- providers/
    |- ui/
|- main.dart
```

## Plugins used
```
  cached_network_image: ^3.1.0
  graphql_flutter: ^5.0.0
  intl: ^0.17.0
  provider: ^5.0.0
  rxdart: ^0.26.0
```
